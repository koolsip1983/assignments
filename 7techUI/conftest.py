from selenium import webdriver
import pytest

@pytest.fixture(scope="session")
def load_driver(request, cpath):
    if cpath:
        web_driver = webdriver.Chrome(cpath)
    else:
        web_driver = webdriver.Chrome()
    web_driver.implicitly_wait(10)
    web_driver.maximize_window()
    session = request.node
    for item in session.items:
        cls = item.getparent(pytest.Class)
        setattr(cls.obj, "driver", web_driver)
    yield
    web_driver.close()

def pytest_addoption(parser):
    parser.addoption("--cpath")

@pytest.fixture(scope="session")
def cpath(request):
    return request.config.getoption("--cpath")