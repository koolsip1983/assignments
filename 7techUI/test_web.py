from selenium import webdriver
import requests
import pytest
from time import sleep
from selenium.webdriver.common.by import By

@pytest.mark.usefixtures("load_driver")
class Test_WEB:
   
    # Assert Broken images 
    def test_case1(self):
        status = True
        driver = self.driver
        URL = "http://the-internet.herokuapp.com/broken_images"
        driver.get(URL)
        image_list = driver.find_elements(By.TAG_NAME, "img")
        print('Total number of images on '+ URL + ' are ' + str(len(image_list)))

        broken_image_count = 0
        for img in image_list:
            try:
                response = requests.get(img.get_attribute('src'), stream=True)
                if (response.status_code != 200):
                    print(img.get_attribute('src') + " is broken.")
                    broken_image_count = (broken_image_count + 1)
                    status = False
            except Exception as e:
                print("Exception occurred:", e)
                status = False
        
        print('The page ' + URL + ' has ' + str(broken_image_count) + ' broken images')
        assert status
    
    #Assert forgot password success message on the page 
    def test_case2(self):
        URL = "http://the-internet.herokuapp.com/forgot_password"
        driver = self.driver
        driver.get(URL)
        id = "email"
        input_elm = driver.find_element_by_id(id)
        input_elm.send_keys("kishankumar83@gmail.com")
        id = "form_submit"
        button_elm = driver.find_element_by_id(id)
        button_elm.click()
        # Site is throwing Error. So cannot automate the Success Message
        assert True
    
    def _login(self, username, password):
        driver = self.driver
        URL = "http://the-internet.herokuapp.com/login"
        driver.get(URL)

        elm_username = driver.find_element_by_id('username')
        elm_password = driver.find_element_by_id('password')
        elm_submit = driver.find_element_by_xpath("//button[@type='submit']")

        elm_username.send_keys(username)
        elm_password.send_keys(password)
        elm_submit.click()

    # Assert form validation functionality Post entering a dummy username and password
    @pytest.mark.parametrize("username, password, output", [
        ('test', 'SuperSecretPassword!', 'Your username is invalid!'),
        ('', 'SuperSecretPassword!', 'Your username is invalid!'),
        ('tomsmith', 'asdf!', 'Your password is invalid!'),
        ])
    def test_case3(self,username, password, output):
        
        self._login(username, password)

        failure_xpath = "//div[@id='flash']"
        error_message = self.driver.find_element_by_xpath(failure_xpath)
        if not error_message:
            assert False

    # Write a test to enter alphabets on this and mark it as a failure if we cannot 
    # enter on page http://the-internet.herokuapp.com/inputs
    def test_case4(self):
        driver = self.driver
        URL = "http://the-internet.herokuapp.com/inputs"
        driver.get(URL)

        xpath = "//div[@id='content']//input"
        input_elm = driver.find_element_by_xpath(xpath)
        data = 'ada'
        input_elm.send_keys(data)
        # incase we can enter alphabets the value atteibute will have the text we entered
        assert(data == input_elm.get_attribute("value"))
    
    #Write a test to sort the table by the amount due on page
    def test_case5(self):
        driver = self.driver
        URL = "http://the-internet.herokuapp.com/tables"
        driver.get(URL)

        rows_loc = "./tbody//td[contains(@class, 'dues')]"
        dues_loc = "./thead//span[contains(@class, 'dues')]"
        
        table = driver.find_element(By.ID, 'table2')

        unsorted_rows = table.find_elements(By.XPATH, rows_loc)
        unsorted_values = [float(row.get_attribute('innerHTML').replace('$', '')) for row in unsorted_rows]
        print(unsorted_values)
        table.find_element(By.XPATH, dues_loc).click()
        sleep(2)
        sorted_rows = table.find_elements(By.XPATH, rows_loc)
        sorted_values = [float(row.get_attribute('innerHTML').replace('$', '')) for row in sorted_rows]
        assert sorted(unsorted_values) == sorted_values
    
    # Right a looped script to assert a 'successful notification" after repeated unsuccessful notification on page
    def test_case5(self):
        driver = self.driver
        URL = "http://the-internet.herokuapp.com/notification_message_rendered"
        driver.get(URL)
        status = False
        while(True):
            click_here = driver.find_element(By.XPATH, "//a[contains(text(),'Click here')]")
            click_here.click()
            flash_message = driver.find_element(By.ID,"flash")
            flashtext = flash_message.text
            if(flashtext.find("Action successful") > -1):
                status = True
                break
            sleep(1)
  
        assert status



