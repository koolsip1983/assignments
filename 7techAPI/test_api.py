import requests
import json
from userAPI import userAPI
import pytest

class TestUser_API:
    userapi = userAPI()
    username = "eve.holt@reqres.in"
    password = "cityslicka"
    userid = 0

    # test the Register API by registering the user successfully 
    # using https://reqres.in/api/register and by logging in using the data used for the registration on API

    def test_case1(self):
        # call the register api and capture the respone. Validate if the registration is 
        # success by validating we are getting a id and token object.

        response = self.userapi.register_user(self.username,self.password)
        assert response["id"] is not None
        assert response["token"] is not None
        self.userid = response["id"]

        response = self.userapi.login_user(self.username,self.password)
        assert response["token"] is not None

    # Using details of the user created in step a, Delete the user registered above and assert an unsuccessful login attempt on login API https://reqres.in/api/login. 
    # Pass it only if the user created in Step a is deleted and unable to login
    @pytest.mark.depends(on=['test_case1'])
    
    def test_case2(self):
        response = self.userapi.delete_user(self.userid)
        assert response.status_code == 204

    #Assert a resource on https://reqres.in/api/unknown where page=1 and ID=2, year=2001
    def test_case3(self):
        pageid = 1
        response = self.userapi.get_single_resource(pageid)
        data = response["data"]
        id = data["id"]
        year = data["year"]
        assert id == 2
        assert year == 2001
    
    #  Assert a user on https://reqres.in/api/users?page=2 
    # where the assertion is passed if the payload contains user with ID=7, Lastname =Lawson
    def test_case4(self):
        response = self.userapi.get_users_list()
        data = response["data"]
        id_firstnamecombination = []
        for objects in data:
            id = objects["id"]
            lastname = objects["last_name"]
            # create a combination of id and lastname in a list. This is required to validate the id lastname combination
            id_firstnamecombination.append(str(id)+":"+lastname)
        
        assert "7:Lawson" in id_firstnamecombination
    
    # Assert the payload in API https://reqres.in/api/users/2 
    # where it will check for first_name as "John" and fails the test if it's not "John"
    def test_case5(self):
        response = self.userapi.get_users_list()
        data = response["data"]
        
        # collect all the firstnames and then assert if it contains John
        firstnamearray = []
        for objects in data:
            firstnamearray.append(objects["first_name"])
        assert "John" in firstnamearray
        
        


        