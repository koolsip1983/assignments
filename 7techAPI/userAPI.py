import requests
import json

class userAPI:

    def register_user(self,username,password):
        payload = "{ \"email\":\""+username+"\",\"password\":\""+password+"\"}"
        response = requests.post("https://reqres.in" + "/api/register", data=payload,headers={'Content-Type': 'application/json'})
        response_body = response.json()
        return response_body
    
    def login_user(self,username,password):
        payload = "{ \"email\":\""+username+"\",\"password\":\""+password+"\"}"
        response = requests.post("https://reqres.in" + "/api/login", data=payload,headers={'Content-Type': 'application/json'})
        response_body = response.json()
        return response_body
    
    def delete_user(self,userid):
        response = requests.delete("https://reqres.in" + "/api/users/"+str(userid),headers={'Content-Type': 'application/json'})
        return response
    
    def get_single_resource(self,pageid):
        response = requests.get("https://reqres.in" + "/api/unknown/"+str(pageid),headers={'Content-Type': 'application/json'})
        return response.json()
    
    def get_users_list(self):
        response = requests.get("https://reqres.in" + "/api/users?page=2",headers={'Content-Type': 'application/json'})
        return response.json()



